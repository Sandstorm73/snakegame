// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "SnakeBorder.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	CreateBorders();
	CreateNewFood();
	start_moving = true;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (start_moving) {
		Move();
	}

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	bool is_new = SnakeElements.Num() == 0;
	for (int i = 0; i < ElementsNum; ++i) {
		
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		if (!is_new) {
	    	ASnakeElementBase* LastSnakeElem = SnakeElements.Last();
			NewLocation = LastSnakeElem->GetActorLocation();
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	float Speed = ElementSize;
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += Speed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= Speed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= Speed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += Speed;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::CreateBorders()
{
	for (int i = 0; i < FIELD_SIZE + 2; i++) {
		FVector NewLocation1(((float)i - FIELD_OFFSET - 1) * ElementSize, ((float)0 - FIELD_OFFSET - 1) * ElementSize, 10);
		FTransform NewTransform1(NewLocation1);
		GetWorld()->SpawnActor<ASnakeBorder>(BorderClass, NewTransform1);

		FVector NewLocation2(((float)0 - FIELD_OFFSET - 1) * ElementSize, ((float)i - FIELD_OFFSET - 1) * ElementSize, 10);
		FTransform NewTransform2(NewLocation2);
		GetWorld()->SpawnActor<ASnakeBorder>(BorderClass, NewTransform2);

		FVector NewLocation3(((float)i - FIELD_OFFSET - 1) * ElementSize, ((float)FIELD_SIZE - FIELD_OFFSET) * ElementSize, 10);
		FTransform NewTransform3(NewLocation3);
		GetWorld()->SpawnActor<ASnakeBorder>(BorderClass, NewTransform3);

		FVector NewLocation4(((float)FIELD_SIZE - FIELD_OFFSET) * ElementSize, ((float)i - FIELD_OFFSET - 1) * ElementSize, 10);
		FTransform NewTransform4(NewLocation4);
		GetWorld()->SpawnActor<ASnakeBorder>(BorderClass, NewTransform4);
	}
}

void ASnakeBase::CreateNewFood()
{
	auto** field = new bool* [FIELD_SIZE]; 
	for (int count = 0; count < FIELD_SIZE; count++)
		field[count] = new bool[FIELD_SIZE];

	for (int i = 0; i < FIELD_SIZE; i++) {
		for (int j = 0; j < FIELD_SIZE; j++) {
			field[i][j] = true;
		}
	}

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		FVector CurrentLocation = SnakeElements[i]->GetActorLocation();

		float x = CurrentLocation.X;
		float y = CurrentLocation.Y;

		int x_index = (int) x / ElementSize + FIELD_OFFSET;
		int y_index = (int) y / ElementSize + FIELD_OFFSET;

		field[x_index][y_index] = false;
	}

	int food_x = 0;
	int food_y = 0;
	bool free_cell = false;

	while (!free_cell) {
		food_x = rand() % FIELD_SIZE;
		food_y = rand() % FIELD_SIZE;

		if (field[food_x][food_y]) {
			free_cell = true;
		}
	}

	FVector NewLocation(((float) food_x - 8) * ElementSize, ((float)food_y - FIELD_OFFSET) * ElementSize, 10);
	FTransform NewTransform(NewLocation);
	GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	
	for (int count = 0; count < FIELD_SIZE; count++)
		delete[] field[count];

	if (speed_increased_level < 0.4) {
		speed_increased_level += 0.04;
		SetActorTickInterval(MovementSpeed - speed_increased_level);
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}